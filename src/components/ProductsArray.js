import React, { useEffect, useReducer, useState } from 'react'
import { Button, Card, Container, Form, Modal } from 'react-bootstrap'
import Swal from 'sweetalert2';

export default function ProductsArray({ productProps }) {
    const {productId, productName, quantity} = productProps

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true)

    const [cart, setCart] = useState([])
    const [quantity2, setQuantity] = useState(0)



    const removeToCart = (e) => {
        e.preventDefault()
        
        fetch(`${process.env.REACT_APP_API_URL}/users/removeToCart/${productId}`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                quantity: +quantity2
            })
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data);
            
            if(data !== false) {
                async function wait() {
                    await Swal.fire({
                        title: "Removed from Cart",
                        icon: "success"
                    })
                    
                }
                wait()
                // fetchData()
                
            } /* else {
                Swal.fire({
                    title: "",
                    icon: "error"
                })
            } */
        })
    }

    /* Try: To refresh the component once the form submits */
    const fetchData = () => {
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails/`, {
            headers: {
                'Content-type' : 'Application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
    }

        useEffect(() => {

            fetchData();
        },[])

        
  return (
    <>
            <Container fluid className='g-0' style={{width: "16rem"}}>
                <Card.Subtitle>Quantity:</Card.Subtitle>
                <Card.Text>{quantity}</Card.Text>
                <Button type="button" variant='danger' onClick={handleShow}>Remove To Cart</Button>
            </Container>

    <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton className='bg-dark'>
            <Modal.Title>
            <p className='h3 text-white'>Confirm Remove?</p>
            </Modal.Title>
        </Modal.Header>
            <Modal.Body>
                <Form onSubmit={removeToCart}>
                    <Form.Control
                        className='mb-3'
                        placeholder="Enter Quantity"
                        type='number'
                        inputMode='numeric'
                        value = {quantity2}
                        onChange={e => setQuantity(e.target.value) }
                        required
                        />
                    <Button type="submit" variant='danger' onClick={handleClose}>Remove to cart</Button>
                </Form>
            </Modal.Body>
        </Modal>
    </>
  )
}
