import React from 'react'
import { Route, Routes, useLocation } from 'react-router'


import Home from '../pages/Home'
import Login from '../pages/Login'
import Logout from '../pages/Logout'
import Register from '../pages/Register'

import { AnimatePresence } from 'framer-motion'
import AdminDashboard from '../pages/AdminDashboard'
import Products from '../pages/Products'
import ProductView from './ProductView'
import UserProfile from '../pages/UserProfile'
import Checkout from '../pages/Checkout'
import UserOrders from '../pages/UserOrders'
import AllOrders from '../pages/AllOrders'

export default function AnimatedRoutes() {
    const location = useLocation()
  return (
    <AnimatePresence>
        <Routes location={location} key={location.pathname}>
            <Route path='/' element={<Home />} />
            <Route path='/products' element={<Products />} />
            <Route path='/login' element={<Login />} />
            <Route path='/logout' element={<Logout />} />
            <Route path='/register' element={<Register />} />
            <Route path='/allOrders' element={<AllOrders />} />
            <Route path='/admin' element={<AdminDashboard />} />
            <Route path='/userProfile' element={<UserProfile />} />
            <Route path='/userProfile/checkout' element={<Checkout />} />
            <Route path='/myOrders' element={<UserOrders />} />
            <Route path='/products/:productId' element={<ProductView />} />
        </Routes>
    </AnimatePresence>
  )
}
