import React from 'react'
import { Card } from 'react-bootstrap'

export default function OrderedProducts({ orderProps }) {
    const {_id, totalAmount, purchasedOn, products} = orderProps
  return (
    <>
        <Card className="my-3">
            <Card.Header className='bg-dark text-light'>
            <p className='h4'>{products[0].productName}</p>
            </Card.Header>
            <Card.Body>
                <Card.Subtitle>Date of Purchase</Card.Subtitle>
                <Card.Text>{purchasedOn}</Card.Text>
                <Card.Subtitle>Total Amount:</Card.Subtitle>
                <Card.Text>{totalAmount}</Card.Text>
            </Card.Body>
        </Card>
    </>
  )
}
