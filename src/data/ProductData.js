import React from 'react'
import { Card } from 'react-bootstrap'

export default function ProductData({ productProps }) {
    const {productId, productName, quantity} = productProps //products []
  return (
    <>
    <Card>
        <Card.Body className='p-0'>
            <Card.Header>Hello</Card.Header>
            <Card.Body>
                <Card.Subtitle>Purchased on:</Card.Subtitle>
                <Card.Text>{productId}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                {/* <Card.Text>{productName}</Card.Text> */}
                <Card.Subtitle>Quantity</Card.Subtitle>
                {/* <Card.Text>{quantity}</Card.Text> */}
            </Card.Body>
        </Card.Body>
    </Card>
    </>
  )
}
