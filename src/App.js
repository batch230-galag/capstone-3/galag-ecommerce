import './App.css';
import {Route, BrowserRouter as Router, Routes } from 'react-router-dom'
import { Container } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'


// Pages
// Components
import AppNavbar from './components/AppNavbar';
import UserProvider from './UserContext';
import AnimatedRoutes from './components/AnimatedRoutes';

function App() {
  return (
    <div className='noScroll'>
    <UserProvider>
    <Router>
        <Container fluid className='g-0'>
        <AppNavbar />
          <AnimatedRoutes />
        </Container>
      </Router>
    </UserProvider>
    </div>
  );
}

export default App;
