import {useContext, useEffect, useState} from "react";
import { Button, Table, Modal, Form, Card, Container } from "react-bootstrap";
import {json, Navigate} from "react-router-dom";
import Swal from "sweetalert2";
import { UserContext } from "../UserContext";

// motion
import { motion } from "framer-motion";


export default function AdminDashboard(){

	const { user } = useContext(UserContext);

	// Create allProducts state to contain the products from the response of our fetch data.
	const [allProducts, setAllProducts] = useState([]);
	const [allUsers, setAllUsers] = useState([]);

	// State hooks to store the values of the input fields for our modal.
	const [productId, setProductId] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
    const [stocks, setStocks] = useState(0);
    const [productImage, setProductImage] = useState("");
    const [type, setType] = useState('');
	// const [filename, setImage] = useState(null);

    // State to determine whether submit button in the modal is enabled or not
    const [isActive, setIsActive] = useState(false);

    // State for Add/Edit Modal
    const [showAdd, setShowAdd] = useState(false);
    const [showUsers, setShowUsers] = useState(false);
	const [showEdit, setShowEdit] = useState(false);

	// To control the add product modal pop out
	const openAdd = () => setShowAdd(true); //Will show the modal
	const closeAdd = () => setShowAdd(false); //Will hide the modal
	const openUsers = () => setShowUsers(true); //Will show the modal
	const closeUsers = () => setShowUsers(false); //Will hide the modal


	/* const handleImageChange = (e) => {
		setImage(e.target.files[0]);
	}; */


	useEffect(() => {
		document.title = "Dashboard"
	})

	// To control the edit product modal pop out
	// We have passed a parameter from the edit button so we can retrieve a specific product and bind it with our input fields.
	const openEdit = (id) => {
		setProductId(id);

		// Getting a specific product to pass on the edit modal
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			// updating the product states for editing
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setStocks(data.stocks);
		});

		setShowEdit(true)
	};

	const closeEdit = () => {

		// Clear input fields upon closing the modal
	    setName('');
	    setDescription('');
	    setPrice(0);
	    setStocks(0);

		setShowEdit(false);
	};

	// [SECTION] To view all product in the database (active & inactive)
	// fetchData() function to get all the active/inactive products.
	const fetchData = () =>{
		// get all the products from the database
		fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`, {
			mode: 'cors',
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data);

			setAllProducts(data.map((product) => {
				return (
						<tr key={product._id}>
							<td>{product._id}</td>
							<td>{product.name}</td>
							<td>{product.description}</td>
							<td>{product.foodType}</td>
							<td>{product.price}</td>
							<td>{product.stocks}</td>
							<td>{product.isActive ? "Active" : "Inactive"}</td>
							<td>
								{
									// conditonal rendering on what button should be visible base on the status of the product
									(product.isActive)
									?
										<Button variant="danger" size="sm" onClick={() => archive(product._id, product.name)}>Archive</Button>
									:
										<>
											<Button variant="success" size="sm" className="mx-1" onClick={() => unarchive(product._id, product.name)}>Unarchive</Button>
											<Button variant="secondary" size="sm" className="mx-1" onClick={() => openEdit(product._id)}>Edit</Button>
										</>

								}
							</td>
						</tr>
				)
			}));
		});
	}

	const fetchUserData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/users/allUsers`, {
			mode: 'cors',
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setAllUsers(data)
		})
	}



	// [SECTION] Setting the product to Active/Inactive

	// Making the product inactive
	const archive = (id, productName) =>{
		console.log(id);
		console.log(productName);

		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Archive Successful",
					icon: "success",
					text: `${productName} is now inactive.`
				});
				// To show the update with the specific operation intiated.
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}
	
	
	// Making the user admin
	const adminTrue = (id, email) =>{
		console.log(id);
		console.log(email);

		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/users/setAdmin/${id}`, {
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isAdmin: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Role Changed",
					icon: "success",
					text: `${email} is now Admin.`
				});
				// To show the update with the specific operation intiated.
				fetchUserData();
			}
			else{
				Swal.fire({
					title: "Archive unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	// Making the product active
	const unarchive = (id, productName) =>{
		console.log(id);
		console.log(productName);

		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Unarchive Successful",
					icon: "success",
					text: `${productName} is now active.`
				});
				// To show the update with the specific operation intiated.
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}


	// Making the user back to guest
	const adminFalse = (id, email) =>{
		console.log(id);
		console.log(email);

		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/users/setAdmin/${id}`, {
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isAdmin: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Role Changed",
					icon: "success",
					text: `${email} is now Guest User.`
				});
				// To show the update with the specific operation intiated.
				fetchUserData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}


	// [SECTION] Adding a new product
	// Inserting a new product in our database
	const addProduct = (e) =>{
			// Prevents page redirection via form submission
		    e.preventDefault();

			/* const formData = new FormData();
			formData.append('image', filename);
			formData.append('name', name);
			formData.append('description', description);
			formData.append('stocks', stocks);
			formData.append('foodType', type);
			formData.append('price', price); */

		    fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
		    	method: "POST",
		    	headers: {
					// "Content-Type": "application/json",
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body:JSON.stringify({
					name: name,
					description: description,
					price: price,
					foodType: type,
					stocks: stocks,
					productImage: productImage 
				})
				
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Product successfully Added",
		    		    icon: "success",
		    		    text: `${name} is now added`
		    		});

		    		// To automatically add the update in the page
		    		fetchData();
		    		// Automatically closed the modal
		    		closeAdd();
		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});
		    		closeAdd();
		    	}

		    })

		    // Clear input fields
		    setName('');
		    setDescription('');
		    setPrice(0);
		    setStocks(0);
		    setType('');
	}

	// [SECTION] Edit a specific product
	// Updating a specific product in our database
	// edit a specific product
	const editProduct = (e) =>{
			// Prevents page redirection via form submission
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
		    	method: "PUT",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    name: name,
				    description: description,
				    price: price,
				    stocks: stocks,
				    productImage: productImage,
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Product succesfully Updated",
		    		    icon: "success",
		    		    text: `${name} is now updated`
		    		});

		    		// To automatically add the update in the page
		    		fetchData();
		    		// Automatically closed the form
		    		closeEdit();

		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});

		    		closeEdit();
		    	}

		    })

		    // Clear input fields
		    setName('');
		    setDescription('');
		    setPrice(0);
		    setStocks(0);
	} 

	// to fetch all products in the first render of the page.
	useEffect(()=>{
		fetchData();
		fetchUserData();
	}, [])

	// Submit button validation for add/edit product
	useEffect(() => {

        // Validation to enable submit button when all fields are populated and set a price and slot greater than zero.
        if(name !== "" && description !== "" && price > 0 && stocks > 0){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, description, price, stocks]);

	return(
		(user.isAdmin)
		?
		<>
		<motion.div className="container-fluid"
			initial={{opacity: 0, display: "none"}}
        	animate={{opacity: 100, display: "block", transition: {duration: 0.5}}}
        	// exit={{rotateY: 180, display: "none"}}
        	>
			{/*Header for the admin dashboard and functionality for create product and show enrollments*/}
			<div className="mt-5 mb-3 text-end">
				<h1>Admin Dashboard</h1>
				{/*Adding a new product */}
				<Button variant="success" className="
				mx-2" onClick={openAdd}>Add Product</Button>
				{/*To view all the user enrollments*/}
				<Button variant="secondary" className="
				mx-2" onClick={openUsers}>Show Users</Button>
			</div>
			{/*End of admin dashboard header*/}

			{/*For view all the products in the database.*/}
			<Table striped bordered hover>
		      <thead>
		        <tr>
		          <th>Product ID</th>
		          <th>Product Name</th>
		          <th>Description</th>
		          <th>Type</th>
		          <th>Price</th>
		          <th>Stocks</th>
		          <th>Status</th>
		          <th>Actions</th>
		        </tr>
		      </thead>
		      <tbody>
	        	{allProducts}
		      </tbody>
		    </Table>
			{/*End of table for product viewing*/}

	    	{/*Modal for Adding a new product*/}
	        <Modal show={showAdd} onHide={closeAdd}>
	    		<Form onSubmit={e => addProduct(e)} encType="multipart/form-data">

	    			<Modal.Header closeButton>
	    				<Modal.Title>Add New Product</Modal.Title>
	    			</Modal.Header>

	    			<Modal.Body>
	    	        	<Form.Group controlId="name" className="mb-3">
	    	                <Form.Label>Product Name</Form.Label>
	    	                <Form.Control 
	    		                type="text" 
	    		                placeholder="Enter Product Name" 
	    		                value = {name}
	    		                onChange={e => setName(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="description" className="mb-3">
	    	                <Form.Label>Product Description</Form.Label>
	    	                <Form.Control
	    	                	as="textarea"
	    	                	rows={3}
	    		                placeholder="Enter Product Description" 
	    		                value = {description}
	    		                onChange={e => setDescription(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

						<Form.Group controlId="type" className="mb-3">
	    	                <Form.Label>Product Type</Form.Label>
	    	                <Form.Select aria-label="Select Product Type"
								id="select"
								value={type}
								onChange={e => setType(e.target.value)}
							>
								<option value=''>Select One</option>
								<option value='drinks'>Drinks</option>
								<option value='biscuits'>Biscuits</option>
								<option value='chips'>Chips</option>
								<option value='candies'>Candies</option>
							</Form.Select>
	    	            </Form.Group>
						
						{/* <Form.Group controlId="formFile" className="mb-3">
							<Form.Label>Default file input example</Form.Label>
							<Form.Control type="file"
							onChange={handleImageChange} />
						</Form.Group>
 */}
	    	            <Form.Group controlId="price" className="mb-3">
	    	                <Form.Label>Product Price</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter Product Price" 
	    		                value = {price}
	    		                onChange={e => setPrice(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="stocks" className="mb-3">
	    	                <Form.Label>Product Stocks</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter Product Slots" 
	    		                value = {stocks}
	    		                onChange={e => setStocks(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

						<Form.Group controlId="name" className="mb-3">
	    	                <Form.Label>Product Url</Form.Label>
	    	                <Form.Control 
	    		                type="text" 
	    		                placeholder="Enter Product Url" 
	    		                value = {productImage}
	    		                onChange={e => setProductImage(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    			</Modal.Body>

	    			<Modal.Footer>
	    				{ isActive 
	    					? 
	    					<Button variant="primary" type="submit" id="submitBtn">
	    						Save
	    					</Button>
	    				    : 
	    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
	    				    	Save
	    				    </Button>
	    				}
	    				<Button variant="secondary" onClick={closeAdd}>
	    					Close
	    				</Button>
	    			</Modal.Footer>

	    		</Form>	
	    	</Modal>
	    {/*End of modal for adding product*/}

    	{/*Modal for Editing a product*/}
        <Modal show={showEdit} fullscreen={true} onHide={closeEdit}>
    		<Form onSubmit={e => editProduct(e)}>

    			<Modal.Header closeButton>
    				<Modal.Title>Edit a Product</Modal.Title>
    			</Modal.Header>

    			<Modal.Body>
    	        	<Form.Group controlId="name" className="mb-3">
    	                <Form.Label>Product Name</Form.Label>
    	                <Form.Control 
    		                type="text" 
    		                placeholder="Enter Product Name" 
    		                value = {name}
    		                onChange={e => setName(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="description" className="mb-3">
    	                <Form.Label>Product Description</Form.Label>
    	                <Form.Control
    	                	as="textarea"
    	                	rows={3}
    		                placeholder="Enter Product Description" 
    		                value = {description}
    		                onChange={e => setDescription(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="price" className="mb-3">
    	                <Form.Label>Product Price</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter Product Price" 
    		                value = {price}
    		                onChange={e => setPrice(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="slots" className="mb-3">
    	                <Form.Label>Product Slots</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter Product Slots" 
    		                value = {stocks}
    		                onChange={e => setStocks(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

					<Form.Group controlId="name" className="mb-3">
	    	                <Form.Label>Product Url</Form.Label>
	    	                <Form.Control 
	    		                type="text" 
	    		                placeholder="Enter Product Url" 
	    		                value = {productImage}
	    		                onChange={e => setProductImage(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>
    			</Modal.Body>

    			<Modal.Footer>
    				{ isActive 
    					? 
    					<Button variant="primary" type="submit" id="submitBtn">
    						Save
    					</Button>
    				    : 
    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
    				    	Save
    				    </Button>
    				}
    				<Button variant="secondary" onClick={closeEdit}>
    					Close
    				</Button>
    			</Modal.Footer>

    		</Form>	
    	</Modal>
		</motion.div>
    	{/*End of modal for editing a product*/}

		{/* Modal for setting Admin to true/false */}
		<Modal show={showUsers} onHide={closeUsers}>
			<Modal.Header closeButton>
				<Modal.Title>Change Roles</Modal.Title>
			</Modal.Header>
			<Modal.Body>
			<Card>
			{
				allUsers.map((users,index) =>
					<Card.Body key={index} className="d-flex">
						<Container>
						<div>Name: {users.firstName} {users.lastName}</div>
						<div>Email: {users.email}</div>
						<div>Admin: {users.isAdmin  ? "True" : "False"} </div>
						</Container>
						<Container>
						{
							// conditional rendering on what button should be visible base on the status of the product
							(users.isAdmin)
							?
								<Button variant="danger" size="sm" onClick={() => adminFalse(users._id, users.email)}>Set To Guest</Button>
							:
								<>
									<Button variant="success" size="sm" className="mx-1" onClick={() => adminTrue(users._id, users.email)}>Set To Admin</Button>
								</>
						}
						</Container>
					</Card.Body>
				)
			}
			</Card>
			</Modal.Body>
			<Modal.Footer>
			</Modal.Footer>
		</Modal>
		</>
		:
		<Navigate to="/" />
	)
}