import React, { useEffect, useState } from 'react'
import { Button, Card, Container } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import ProductsArray from '../components/ProductsArray';

export default function Checkout() {

    const [myCart, setMyCart] = useState([])
    const [myOrder, setMyOrder] = useState([])
    const [sum, setSum] = useState(0)
    const navigate = useNavigate()  

    const checkPrice = () => {
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails/`, {
        headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data)
            setMyCart(data.orderedProduct)
        
            const totalAmountArray = myCart.map(ta => ta.totalAmount)
        
            const amountArray = totalAmountArray.length
            let total = 0
        
            for(let i = 0; i <amountArray; i++ ) {
                total += totalAmountArray[i];
            }
            setSum(total)
        })


    }    

    useEffect(() => {
        checkPrice()
    }, [myCart])

    const ConfirmCheckout = () => {
    fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json())
    .then(data => {
        // console.log(data);
        
        if(data !== false) {
            async function wait() {
                await Swal.fire({
                    title: "Thank you for purchasing!",
                    icon: "success"
                })
                navigate("/userProfile")
                
            }
            wait()
            
        } /* else {
            Swal.fire({
                title: "",
                icon: "error"
            })
        } */
    })
}


    return (
        <>
        <Container fluid className='d-flex flex-wrap justify-content-xl-start justify-content-center gap-2'>
            {
                myCart.map((user, index) =>
                    <div key={index} className="my-3">
                        <p className='h4'>{user.products[0].productName}</p>
                        <div>
                            <div>Date Added:</div>
                            <div>{user.purchasedOn}</div>
                            <Card.Subtitle>Total Amount:</Card.Subtitle>
                            <Card.Text>{user.totalAmount}</Card.Text>
                            {
                                user.products.map((prod, num) =>
                                <ProductsArray key={prod.productId} productProps={prod} />
                                    
                                )
                            }
                        </div>
                    </div>
                )
            }
        </Container>

        <Container fluid className='fixed-bottom gx-0'>
            <Card className='py-5 px-3'>
                <p className='h3'>Total Price: {sum}</p>
            <Button onClick={ConfirmCheckout}>Confirm Checkout</Button>
            </Card>
        </Container>
        </>
    )
}
