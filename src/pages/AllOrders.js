import React, { useEffect, useState } from 'react'
import { Badge, Button, Card, Col, Container, Row, Table, Toast, ToastContainer } from 'react-bootstrap'
import Swal from 'sweetalert2';

export default function AllOrders() {
    const [orders, setOrders] = useState([])
    const [name, setName] = useState('')
    
    const [show, setShow] = useState(false);
    const [date, setDate] = useState(Date)



    useEffect(() => {
        fetchOrderData();
    }, [])

    const handleDelivered = (orderId, name, show) =>{
		console.log(orderId);
		console.log(name);
        setShow(show)
        setName(name)

		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/orders/isDelivered/${orderId}`, {
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
            body: JSON.stringify({
                isDelivered: true
            })
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
            // setShow(true);

			if(data){
				/* Swal.fire({
					title: "Archive Successful",
					icon: "success",
					text: `${name} is now inactive.`
				}); */

				// To show the update with the specific operation intiated.
				fetchOrderData();
			}
			else{
				Swal.fire({
					title: "Archive unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

    const fetchOrderData = () => {
        fetch(`${process.env.REACT_APP_API_URL}/orders/allOrders`, {
            mode: 'cors',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setOrders(data)
        })
        

    }


return (
    <>
    <ToastContainer position='top-end' containerPosition='fixed'>
                    <Toast onClose={() => setShow(false)} show={show} delay={3000} autohide>
                        <Toast.Header>
                            <img
                            src="holder.js/20x20?text=%20"
                            className="rounded me-2"
                            alt=""
                            />
                            <strong className="me-auto">Delivered!</strong>
                            <small>{date}</small>
                        </Toast.Header>
                        <Toast.Body>Woohoo, {name} is Delivered</Toast.Body>
                    </Toast>
                </ToastContainer>

    <Container className='d-flex flex-column flex-wrap'>
    {
        orders.map((user, index) =>
            <Card key={index} className="my-3">
            <Card.Header className=''>
                <p className='h4'>Order Id: {user._id}</p>
            </Card.Header>
            <Card.Header className='bg-dark text-light'></Card.Header>
                <Card.Body>
                <Container fluid className='d-flex'>
                    <Container fluid>
                    <Card.Subtitle>Name</Card.Subtitle>
                    <Card.Text>{user.name}</Card.Text>
                    <Card.Subtitle>Date Purchased</Card.Subtitle>
                    <Card.Text>{user.purchasedOn}</Card.Text>
                    <Card.Subtitle>Total Amount:</Card.Subtitle>
                    <Card.Text>{user.totalPrice}</Card.Text>
                    </Container>   
                    <Container>
                    {user.isDelivered ?
                            <p className='h5'>
                            <Badge bg='success' className='p-2'>Delivered</Badge>
                            </p>
                            :
                            <>
                            <p className='h5'>
                            <Badge bg='danger' className='p-2'>Pending</Badge>
                            </p>
                            <Button onClick={() => handleDelivered(user._id, user._id, true)} variant="info">Order Delivered?</Button>
                            </>
                        }
                    </Container>   
                </Container>
                    <Container className='p-2' >
                        <Container className=''>
                            {
                                user.orders.map((prod, num) =>
                                    <Table fluid="true" key={prod._id} striped bordered hover variant='info' className=''>
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Quantity</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{prod.productName}</td>
                                                <td>{prod.quantity}</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                )
                            }
                        </Container>
                    </Container>
                </Card.Body>
            </Card>
        )
    }
    </Container>
    </>
  )
}
