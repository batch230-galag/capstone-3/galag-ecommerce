import { Fragment, useContext, useEffect, useState } from 'react'
import { Button, Card, Container, Form, Image, Tab, Tabs } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'
import ProductsCard from '../components/ProductsCard'
import { UserContext } from '../UserContext'
import { easeInOut, motion } from 'framer-motion'

export default function Products() {
    const { user } = useContext(UserContext)
    const [products, setProducts] = useState([])
    const navigate = useNavigate()


    const [data, setData] = useState("");
    const [tabValue, setTabValue] = useState(0);

    
    useEffect(() => {
        document.title = "Products"
    })

    // For Filtering per Type
    const [filterParam, setFilterParam] = useState(["All"])
    // const menuItems = [...new Set(data.map((Val) => Val.foodType))];



    // Search Functionality
    const [search, setSearch] = useState('')
    const [searchParam] = useState(["name"])
    const [searchParam2] = useState(["foodType"])


    /* function searchProduct(products) {
        return products.filter((products) => {
            return searchParam.some((newProduct) => {
                return (
                    products[newProduct]
                        .toString()
                        .toLowerCase()
                        .indexOf(search.toLowerCase()) > -1
                );
            });
        });
    } */

    function searchProduct(products) {
        return products.filter((products) => {

            if(filterParam === "All") {
                return searchParam.some((newProduct) => {
                    return (
                        products[newProduct]
                            .toString()
                            .toLowerCase()
                            .indexOf(search.toLowerCase()) > -1
                    );
                });
            } else if (products.foodType === filterParam) {
                return searchParam2.some((newProduct) => {
                    return (
                        products[newProduct]
                            .toString()
                            .toLowerCase()
                            .indexOf(search.toLowerCase()) > -1
                    );
                });
            }
        });
    }


    // FETCH ALL PRODUCTS
    useEffect(() => {
            fetch(`${process.env.REACT_APP_API_URL}/products/allActiveProducts`)
            .then(res => res.json())
            .then(data => {
                setProducts(data)
                setData(data)
            })
            
	}, [])


  return (
    <>

    {(user.id !== null)
    ?
        <>






        <Container fluid>
            <Container fluid className='my-3 d-flex gap-3 justify-content-end align-item-center'>
                <div>
                <Card.Subtitle className="sr-only mb-2">Search Products here</Card.Subtitle>
                <Form.Label>
                    <Form.Control
                        type="search"
                        name="search-form"
                        id="search-form"
                        className="search-input"
                        placeholder="Search for..."
                        value={search}
                        onChange={(e) => setSearch(e.target.value)}
                    />
                </Form.Label>
                </div>
                <div>
                <Card.Subtitle className="sr-only mb-2">Filter Products here</Card.Subtitle>
                    <Form.Select
                        multiple={true}
                        value={filterParam}
                        onChange={(e) => {setFilterParam(e.target.value)}}
                        className="custom-select"
                        aria-label="Filter Countries By Type">
                            <option value="All">All</option>
                            <option value="drinks">Drinks</option>
                            <option value="biscuits">Biscuits</option>
                            <option value="chips">Chips</option>
                            <option value="candies">Candies</option>
                    </Form.Select>
                </div>
            </Container>
        

            <Card>
            <Card.Header className='mb-3 bg-dark text-white'>
                <h1>All Products</h1>
            </Card.Header>

            <Container fluid className='d-flex flex-wrap justify-content-xl-start justify-content-center gap-2'>
            {/* SearchBar Functionality */}
            {
                searchProduct(products).map((products, i) => {
                    return (
                        <motion.div key={products._id}
                        
                            whileHover={{
                                scale: 1.1,
                                // transition:{ type: 'spring', stiffness:400, damping:10}
                                }}
                            initial={{opacity: 0, translateY: 100}}
                            animate={{opacity: 1, translateY: 0, 
                                transition:{duration: 0.3, delay: i * 0.1}
                            }}
                            >
                            <ProductsCard key={products._id} productProp={products}  />
                        </motion.div>

                    )    
            })
            }
            </Container>

            </Card>
        </Container>
        </>
    :
        <h1>YOU NEED TO LOGIN</h1>
    }
    </>
  )
}
