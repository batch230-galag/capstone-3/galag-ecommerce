import React, { useContext, useEffect, useState } from 'react'
import { Button, Card, Container, Form, InputGroup } from 'react-bootstrap'
import { useNavigate } from 'react-router'
import Swal from 'sweetalert2'
import { UserContext } from '../UserContext'
import { delay, motion } from 'framer-motion'

export default function Login() {
    // setting State for Login
    const {user, setUser} = useContext(UserContext)
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const navigate = useNavigate()

    useEffect(() => {
        document.title = "Login"
    })

    function handleSubmit(e) {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                'Content-type' : 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(typeof data.access !== 'undefined') {
                localStorage.setItem('token', data.access)
                retrieveUserDetails(data.access)

                    if(data.isAdmin !== false) {
                        async function wait() {
                            await Swal.fire({
                                title: "Hello Admin!",
                                icon: "success",
                                text: "Welcome to Zuitt!"
                            })                
                            navigate('/admin')
                        }
                        wait()
                    } else {
                        async function wait() {
                            await Swal.fire({
                                title: `Hello guest!`,
                                icon: "success",
                                text: "Welcome to Zuitt!"
                            })                
                            navigate('/products')
                        }
                        wait()               
                    }

                setEmail('')
                setPassword('')

            } else {
                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Check your login details and try again."
                })

                navigate('/login')

            }
        })
        .catch(error => console.log(error))
    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            localStorage.setItem('email', data.email)
            localStorage.setItem('_id', data._id)
            localStorage.setItem('isAdmin', data.isAdmin)

            setUser({
                email: data.email,
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

  return (
    <motion.div
        initial={{rotateY: 180, display: "none"}}
        animate={{rotateY: 360, display: "block", transition: {duration: 0.5}}}
        exit={{rotateY: 180, display: "none"}}
        >
    <Container className='col-md-4'>
    <Card className='mx-auto mt-5'>
        <Card.Header className='bg-dark text-light'>
            <h1 className='h1 text-center'>Login</h1>
        </Card.Header>
        <Container className='p-3'>
            <Form className='p-3' onSubmit={handleSubmit}>
                <Form.Group controlId='userEmail' className='mb-3'>
                    <Form.Label>Email</Form.Label>
                    <InputGroup>
                    <InputGroup.Text id="basic-addon1">@</InputGroup.Text>
                    <Form.Control
                        type='email'
                        placeholder= "Enter Email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        required />
                    </InputGroup>
                </Form.Group>
                
                <Form.Group controlId='userPassword' className='mb-3'>
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type='password'
                        placeholder= "Enter Password"
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        required />
                </Form.Group>
                <Button type='submit' variant='primary'>Submit</Button>
            </Form>
        </Container>
    </Card>
    </Container>
    </motion.div>
  )
}
